package entities;

import java.io.IOException;
import java.io.Serializable;
import java.util.Comparator;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Locale;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;


@Entity
@NamedQueries(@NamedQuery(name="findAll", query="select a from Action a"))
public class Action implements Serializable, Comparable<Action> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private String title, description, urlImage, location;
	private Date dateBegin, dateEnd;
	private double amount;
	private boolean collectif, isConfirm;
	
	@Lob
	private byte[] imageTitre;
	
	public Action() {}
	
	public Action(String title, String description, String urlImage, Date date1, Date date2, String location, double amount, boolean collectif, boolean isConfirm) {
		this.title = title;
		this.description = description;
		this.urlImage = urlImage;
		this.dateBegin = date1;
		this.dateEnd = date2;
		this.location = location;
		this.amount = amount;
		this.collectif = collectif;
		this.isConfirm = isConfirm;
		try {
			this.imageTitre = this.extractBytes(urlImage);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Action(String title, String description, Date date1, Date date2, String location, double amount, boolean collectif, boolean isConfirm) {
		this.title = title;
		this.description = description;
		this.dateBegin = date1;
		this.dateEnd = date2;
		this.location = location;
		this.amount = amount;
		this.collectif = collectif;
		this.isConfirm = isConfirm;
	}

	public Action(String titre) {
		this.title = titre;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getUrlImage() {
		return urlImage;
	}
	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}

	public Date getDateBegin() {
		return dateBegin;
	}
	
	public String getDateBeginString() {
		String timeStamp = new SimpleDateFormat("EEE dd MMM hh:mm:ss yyyy",
		        Locale.FRANCE).format(this.getDateBegin());
		return timeStamp;
	}
	
	public String getDateEndString() {
		String timeStamp = new SimpleDateFormat("EEE dd MMM hh:mm:ss yyyy",
		        Locale.FRANCE).format(this.getDateEnd());
		return timeStamp;
	}

	public void setDateBegin(Date dateBegin) {
		this.dateBegin = dateBegin;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public boolean isCollectif() {
		return collectif;
	}

	public void setCollectif(boolean collectif) {
		this.collectif = collectif;
	}

	@Override
	public String toString() {
		return "Action [titre=" + title + ", description=" + description + ", du " + dateBegin + " au " + dateEnd+", localité=" +location+", montant=" + amount
				+ "€, collectif=" + collectif + "]";
	}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof Action) {
			return ((Action)o).getTitle().equals(title);
		}
		return false;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isConfirm() {
		return isConfirm;
	}

	public void setConfirm(boolean isConfirm) {
		this.isConfirm = isConfirm;
	}
	
	public String getImageTitre() {
		byte[] tmp = Base64.getEncoder().encode(this.imageTitre);
		return new String(tmp);
		
	}

	public void setImageTitre(byte[] imageTitre) {
		this.imageTitre = imageTitre;
	}
	
	public byte[] extractBytes (String ImageName) throws IOException 
	{
        Path path = Paths.get(ImageName);
         return Files.readAllBytes(path);
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public int compareTo(Action a1) {
		//Comparaison sur l'amount
		/*int compAnnee = Double.valueOf(this.amount).compareTo(Double.valueOf(a1.amount));
        if(compAnnee != 0) { return compAnnee; }*/
      //Comparaison sur la localite
        int compLocation = this.getLocation().compareTo(a1.getLocation());
        if(compLocation != 0) { return compLocation; }
		return 0;
	}

	
}
