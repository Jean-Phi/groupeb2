package util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import sessionejb.IGestionUserEJBRemote;

import entities.Action;
import entities.User;
import entities.Usergroup;
import sessionejb.IGestionActionEJBRemote;

public class Main {

	public static void main(String[] args) throws ParseException {
		/*INITIALISER LA DB*/
		/*
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("groupeB2");
		EntityManager em=emf.createEntityManager();
		EntityTransaction tx = em.getTransaction();
		String string = "January 2, 2010";
		DateFormat formatDate1 = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		Date dateDeb1 = formatDate1.parse(string);
		String string2 = "November 17, 2018";
		DateFormat formatDate2 = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		Date dateFin1 = formatDate2.parse(string2);
		//Action a = new Action("Un GSM pour Julie","Comme vous le savez Julie a skété son nouveau GSM, nous organisons une collection afin de lui en payer un nouveau","C:\\Users\\Cabello\\Downloads\\Citron_vert.jpg",dateDeb1, dateFin1,"Montpellier",400,true, true);
		//Action a2 = new Action("Une nouvelle vie pour Nolan","Comme vous le savez Nolan est malade","C:\\Users\\Cabello\\Downloads\\parapluie-jaune.jpg",dateDeb1, dateFin1,"Charleroi",400,true, true);
		Action a = new Action("Un GSM pour Julie","Comme vous le savez Julie a skété son nouveau GSM, nous organisons une collection afin de lui en payer un nouveau","C:\\Users\\julie\\OneDrive\\Documents\\Citron_vert.jpg",dateDeb1, dateFin1,"Montpellier",400,true, true);
		Action a2 = new Action("Une nouvelle vie pour Nolan","Comme vous le savez Nolan est malade","C:\\Users\\julie\\OneDrive\\Documents\\parapluie-jaune.jpg",dateDeb1, dateFin1,"Charleroi",400,true, true);
		User u = new User("admin","helha");
		User u2 = new User("user","helha");
	//	Action a = new Action("Un GSM pour Julie","Comme vous le savez Julie a skété son nouveau GSM, nous organisons une collection afin de lui en payer un nouveau","C:\\Users\\alexa\\Desktop\\Gif\\Bilal2.jpg",dateDeb1, dateFin1,"Montpellier",400,true, true); //img
	//	Action a2 = new Action("Une nouvelle vie pour Nolan","Comme vous le savez Nolan est malade","C:\\Users\\alexa\\Desktop\\Gif\\LAMA.jpg",dateDeb1, dateFin1,"Charleroi",400,true, true);// imge
		Usergroup ug1 = new Usergroup(u.getLogin(), u.getLogin());
		Usergroup ug2 = new Usergroup(u2.getLogin(), u2.getLogin());

		
		tx.begin();
		em.persist(u);
		em.persist(u2);
		em.persist(a);
		em.persist(a2);
		em.persist(ug1);
		em.persist(ug2);
		tx.commit();
		em.clear();
		em.close();
		emf.close();*/
		
		Context context = null;
		try {
			context = new InitialContext();
		} catch (NamingException e) {
			e.printStackTrace();
		}

		IGestionUserEJBRemote userBean = null;
		IGestionActionEJBRemote actionBean = null;

		try {
			//Portable JNDI names for EJB GestionUserEJB: [java:global/groupeB2/GestionUserEJB!sessionejb.GestionUserEJB, java:global/groupeB2/GestionUserEJB!sessionejb.IGestionUserEJBRemote]
			userBean = (IGestionUserEJBRemote) context.lookup("java:global/groupeB2/GestionUserEJB!sessionejb.IGestionUserEJBRemote");
			//Portable JNDI names for EJB GestionActionEJB: [java:global/groupeB2/GestionActionEJB!sessionejb.GestionActionEJB, java:global/groupeB2/GestionActionEJB!sessionejb.IGestionActionEJBRemote]
			actionBean = (IGestionActionEJBRemote) context.lookup("java:global/groupeB2/GestionActionEJB!sessionejb.IGestionActionEJBRemote");
			} catch (NamingException e) {
			e.printStackTrace();
		}

		System.out.println("USERS:\n"+userBean.selectAll());
		int code;
		Scanner scan = new Scanner(System.in);
		printMenu();
		code = scan.nextInt();

		while(code != 0) {
			switch (code){
			case 20 : 
				//LIST ACTION
				System.out.println("ACTIONS:\n"+actionBean.selectAll());
				break;
			case 21 : 
				//ADD ACTION
				//User admin = userBean.get("admin", "helha");
				String title, description, dateD, dateE, urlI;
				System.out.println("Entrez le titre de l'Action");
				scan.nextLine();
				title = scan.nextLine();
				System.out.println("Titre : "+title);
				System.out.println("Entrez la description de l'Action");
				description = scan.nextLine();
				System.out.println("Description : "+description);
				System.out.println("Entrez l'URL de l'image de l'Action (format .jpg)");
				urlI = scan.nextLine();
				System.out.println("URL : "+urlI);
				System.out.println("Entrez la date de début de l'Action (MMMM d, yyyy)");
				dateD = scan.nextLine();
				DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
				Date date1 = format.parse(dateD);
				System.out.println("Date : "+date1);
				System.out.println("Entrez la date de fin de l'Action (MMMM d, yyyy)");
				dateE = scan.nextLine();
				DateFormat format2 = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
				Date date2 = format2.parse(dateE);
				System.out.println("Date : "+date2);
				System.out.println("Entrez la localité de l'Action");
				String loc = scan.nextLine();
				System.out.println("Localité : "+loc);
				System.out.println("Entrez le montant de l'Action");
				double montant = scan.nextDouble();
				if(montant<20) {
					System.out.println("Le montant minimum est de 20€.");
					montant = 20.;
				}
				System.out.println("Montant : "+montant);
				System.out.println("L'Action est collective ? (O/N)");
				boolean collectif = ("O").equals(scan.next().toUpperCase());
				System.out.println("Collective : "+collectif);
				Action tmpAction = new Action(title, description, urlI, date1, date2, loc, montant, collectif, true);
				System.out.println("\n"+tmpAction);
				System.out.println("Confirmez l'ajout? (O/N)");
				if(("O").equals(scan.next().toUpperCase())) {
					actionBean.addAction(tmpAction);
				}
				break;
			case 22 : 
				//DELETE ACTION
				System.out.println("Entrez l'ID de l'action à supprimer");
				actionBean.deleteAction(scan.nextInt());
				break;
			case 23 : 
				//EDIT ACTION
				System.out.println("Entrez l'ID de l'Action à modifier");
				Action tmpActionToModif = actionBean.get(scan.nextInt());
				
				String titleToModifi, descriptionToModif, dateToModif1, dateToModif2, urlToModif;
				System.out.println("Voulez-vous modifier le titre ? (O/N)");
				if(("O").equals(scan.next().toUpperCase())) {
					System.out.println("Entrez le titre de l'Action");
					scan.nextLine();
					titleToModifi = scan.nextLine();
					System.out.println("Titre : "+titleToModifi);
					tmpActionToModif.setTitle(titleToModifi);
				}
				System.out.println("Voulez-vous modifier la description ? (O/N)");
				if(("O").equals(scan.next().toUpperCase())) {
					System.out.println("Entrez la description de l'Action");
					scan.nextLine();
					descriptionToModif = scan.nextLine();
					System.out.println("Description : "+descriptionToModif);
					tmpActionToModif.setDescription(descriptionToModif);
				}
				System.out.println("Voulez-vous modifier l'image ? (O/N)");
				if(("O").equals(scan.next().toUpperCase())) {
					System.out.println("Entrez l'URL de l'image de l'Action (.jpg)");
					scan.nextLine();
					urlToModif = scan.nextLine();
					System.out.println("URL : "+urlToModif);
					tmpActionToModif.setUrlImage(urlToModif);
				}
				System.out.println("Voulez-vous modifier les dates ? (O/N)");
				if(("O").equals(scan.next().toUpperCase())) {
					System.out.println("Entrez la date de début de l'Action");
					scan.nextLine();
					dateToModif1 = scan.nextLine();
					DateFormat format3 = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
					Date date3 = format3.parse(dateToModif1);
					System.out.println("Date : "+date3);
					tmpActionToModif.setDateBegin(date3);
					System.out.println("Entrez la date de fin de l'Action");
					scan.nextLine();
					dateToModif2 = scan.nextLine();
					DateFormat format4 = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
					Date date4 = format4.parse(dateToModif2);
					System.out.println("Date : "+date4);
					tmpActionToModif.setDateBegin(date4);
				}
				String locToModif;
				System.out.println("Voulez-vous modifier la localité ? (O/N)");
				if(("O").equals(scan.next().toUpperCase())) {
					System.out.println("Entrez la localité de l'Action");
					locToModif = scan.nextLine();
					System.out.println("Montant : "+locToModif);
					tmpActionToModif.setLocation(locToModif);
				}
				double montantToModif;
				System.out.println("Voulez-vous modifier le montant ? (O/N)");
				if(("O").equals(scan.next().toUpperCase())) {
					System.out.println("Entrez le montant de l'Action");
					montantToModif = scan.nextDouble();
					System.out.println("Montant : "+montantToModif);
					tmpActionToModif.setAmount(montantToModif);
				}
				boolean collectifToModif;
				System.out.println("Voulez-vous modifier le type d'action ? (O/N)");
				if(("O").equals(scan.next().toUpperCase())) {
					System.out.println("L'Action est collective ? (O/N)");
					collectifToModif = ("O").equals(scan.next().toUpperCase());
					System.out.println("Collective : "+collectifToModif);
					tmpActionToModif.setCollectif(collectifToModif);
				}
				System.out.println("\n"+tmpActionToModif);
				System.out.println("Confirmez les modifications ? (O/N)");
				if(("O").equals(scan.next().toUpperCase())) {
					actionBean.updateAction(tmpActionToModif);
				}
				break;
			case 24 : 
				//DETAIL ACTION
				System.out.println("Entrez l'ID de l'action à afficher");
				System.out.println(actionBean.get(scan.nextInt()));
				break;
			default:
				System.out.println("INVALID CODE");
				break;
			}
			printMenu();
			code = scan.nextInt();
		}
		System.out.println("Goodbye");
		scan.close();
	}

	//AFFICHE LE MENU
	public static void printMenu() {
		System.out.println("0 : EXIT");
		System.out.println("20 : LIST ACTION");
		System.out.println("21 : ADD ACTION");
		System.out.println("22 : DELETE ACTION");
		System.out.println("23 : EDIT ACTION");
		System.out.println("24 : DETAIL ACTION");
	}


}