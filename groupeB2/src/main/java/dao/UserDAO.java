package dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entities.Action;
import entities.User;

@Stateless
@LocalBean
public class UserDAO{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@PersistenceContext(unitName="groupeB2_JTA")
	private EntityManager em;
	
	public UserDAO() {
		
	}
	
	public List<User> getUsers() {
		List<User> list = new ArrayList<User>();
		String User = "SELECT u FROM User u";
		Query queryUsers = em.createQuery(User);
		list = (List<User>) queryUsers.getResultList();
		return list;
	}
	
	//ATTENTION SINGLE RESULT !
	public User getUserById(Integer id) {
		String User = "SELECT u FROM User u WHERE u.id=:varId";
		Query queryEvent = em.createQuery(User);
		queryEvent.setParameter("varId", id);
		return (User) queryEvent.getSingleResult();
	}
	
	//ATTENTION SINGLE RESULT !
	public User getUserByLogin(String log, String psw) {
		String User = "SELECT u FROM User u WHERE u.login=:varLogin and u.password=:varPsw";
		Query queryEvent = em.createQuery(User);
		queryEvent.setParameter("varLogin", log);
		queryEvent.setParameter("varPsw", psw);
		return (User) queryEvent.getSingleResult();
	}
	
	public boolean addUser(User u) {		
		em.persist(u);
		return true;
	}

	public boolean updateUser(User u) {
		if(u == null)
			return false;
		else {
			User toUpdate = em.find(User.class, u.getId());
			toUpdate.setLogin(u.getLogin());
			toUpdate.setPassword(u.getPassword());
			em.merge(toUpdate);
			return true;
		}
	}
	
	public int searchIdByLogin(String login) {
		String requeteIdByLogin = "SELECT u.id FROM User u where u.login =:name";
		Query queryProprio = em.createQuery(requeteIdByLogin);
		queryProprio.setParameter("name", login);
		
		if(!queryProprio.getResultList().isEmpty()) {
			int id = Integer.parseInt(queryProprio.getResultList().get(0).toString());
			return id;
		}
		return -1;
	}
	
	public boolean deleteUserId(Integer id) {
		if(id>=0) {
			User aSupp = em.find(User.class, id);
			em.remove(aSupp);
			return true;
		}
		return false;
	}
}
