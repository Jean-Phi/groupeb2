package dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entities.Action;

@Stateless
@LocalBean
public class ActionDAO {

	@PersistenceContext(unitName="groupeB2_JTA")
	private EntityManager em;

	public ActionDAO() {
	}
	
	public boolean isExistingTitle(String title) {
		Action a = new Action(title); 
		return !getActionByTitle(a).isEmpty();
	}	
	
	public List<Action> getActionByTitle(Action a){
		List<Action> result = new ArrayList<Action>();

		String selectp = "SELECT a from Action a WHERE a.title =:pTitle";
		Query query = em.createQuery(selectp);
		query.setParameter("pTitle", a.getTitle());
		result = (List<Action>) query.getResultList();
		return result;
	}
	
	//ATTENTION SINGLE RESULT !
	public Action getActionById(Integer id) {
		String Action = "SELECT a FROM Action a WHERE a.id=:varId";
		Query queryEvent = em.createQuery(Action);
		queryEvent.setParameter("varId", id);
		return (Action) queryEvent.getSingleResult();
	}
	
	public boolean addAction(Action a) {
		if(!isExistingTitle(a.getTitle())) {
			em.persist(a);
			return true;
		}
		return false;
	}
	
	public boolean deleteActionId(Integer id) {
		if(id>=0) {
			Action aSupp = em.find(Action.class, id);
			em.remove(aSupp);
			return true;
		}
		return false;
	}
	
	public boolean updateAction(Action a) {
		if(a == null)
			return false;
		else {
			Action toUpdate = em.find(Action.class, a.getId());
			toUpdate.setTitle(a.getTitle());
			toUpdate.setDescription(a.getDescription());
			toUpdate.setDateBegin(a.getDateBegin());
			toUpdate.setDateEnd(a.getDateEnd());
			toUpdate.setLocation(a.getLocation());
			toUpdate.setAmount(a.getAmount());
			toUpdate.setCollectif(a.isCollectif());
			//toUpdate.setUrlImage(a.getImageTitre());
			em.merge(toUpdate);
			return true;
		}
	}
	
	public List<Action> selectAllActions() {
		List<Action> actions = new ArrayList<Action>();
		//String selectp = "SELECT a from Action a ";
		Query query = em.createNamedQuery("findAll");
		actions = (List<Action>) query.getResultList();
		return actions;
	}

	public List<Action> getActionsCarrousel() {
		List<Action> actions = new ArrayList<Action>();
		String selectp = "SELECT a from Action a ORDER BY a.dateBegin asc";
		Query query = em.createQuery(selectp);
		actions = (List<Action>) query.getResultList();
		return actions;
	}
	
}
