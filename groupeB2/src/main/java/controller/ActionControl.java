package controller;

import java.util.ArrayList;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.Collections;

import javax.servlet.http.Part;

import org.glassfish.grizzly.http.server.util.SimpleDateFormats;

import entities.Action;
import sessionejb.GestionActionEJB;

@Named
@RequestScoped
public class ActionControl {

	@EJB
	private GestionActionEJB gestionActionEJB;


	private static double amount;
	private static boolean isAdded,isDelete;
	private static String title, description,urlImage,location,locationBis;
	private static Date date1, date2;
	private static boolean collectif;
	private static boolean confirmed, boolLocalite;
	private static boolean croissant, decroissant;
	private static Action action, actionToUpdate, actionLocation;
	private static Part partImg;
	private static List<Action> actions;
	private static List<Action> actionsLocation = new ArrayList<>();
	
	public List<Action> selectAll() {
		List<Action> actions = new ArrayList<>();
		List<Action> actionsLocation = new ArrayList<>();
		actions = gestionActionEJB.selectAll();
		if(actionLocation != null &&  boolLocalite == false) {
			actionsLocation.clear();
			for(int i=0 ; i< actions.size();i++) {
				if(actionLocation.getLocation().equals(actions.get(i).getLocation())) {
					actionsLocation.add(actions.get(i));
				}
			}
			if(croissant) {
				Collections.sort(actionsLocation);
			}
			if(decroissant) { 
				Collections.sort(actionsLocation);
				Collections.reverse(actionsLocation);
			}
			return actionsLocation;
		}else {
			if(croissant) {
				Collections.sort(actions);
			}
			if(decroissant) { 
				Collections.sort(actions);
				Collections.reverse(actions);
			}
			return actions;
		}
	}

	public void getLocaliteBis(String loc) {
		locationBis = loc;
	}
	public boolean getBoolLocalite(boolean loc) {
		boolLocalite = loc;
		return boolLocalite;
	}
	public boolean croissantOrder() {
		decroissant = false;
		croissant = true;
		return croissant;
	}
	public boolean decroissantOrder() {
		croissant = false;
		decroissant = true;
		return decroissant;
	}
	public List<String> listLocation(){
		int i=0;
		List<Action> actions = gestionActionEJB.selectAll();
		List<String> locations = new ArrayList<>();
		for(i=0; i<actions.size();i++) {
			Action a = actions.get(i);
			String l = a.getLocation();
			if(!locations.contains(l)) {
				locations.add(l);
			}
		}
		return locations;
	}
	public void actionLocation(Action a){
		boolLocalite = false;
		actionLocation = a;
	}

	public boolean doAddAction() {
		// User user = gestionUserEJB.get("admin", "helha");
		Action action = new Action(title,description,urlImage,date1,date2,location,amount,collectif, confirmed);
		// user.addAction(action);
		// isAdded = gestionUserEJB.updateUser(user);
		isAdded = gestionActionEJB.addAction(action);

		return isAdded;
	}

	public String doUserAddAction() {
		Action a = new Action(title, description, date1, date2, location, amount, collectif, false);
		InputStream input;
		try {
			input = partImg.getInputStream();
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			byte[] buffer = new byte[10240];
			for (int length = 0; (length = input.read(buffer)) > 0;) output.write(buffer, 0, length);
			a.setImageTitre(output.toByteArray());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		isAdded = gestionActionEJB.addAction(a);
		title=""; description=""; urlImage=""; 
		//date1=null; date2=null;
		date1 = new Date(); date2= new Date();
		amount=0;
		collectif=false;
		return "listAction.xhtml";
	}

	public boolean doDeleteAction(Integer id) {
		isDelete = gestionActionEJB.deleteAction(id);
		return isDelete;
	}

	public String beforeUpdateAction(Action a) {
		actionToUpdate = a;
		return "updateAction.xhtml";
	}

	public String doUpdateAction() {
		gestionActionEJB.updateAction(actionToUpdate);
		return "listAction.xhtml";
	}

	public String doDetailsAction(Action a) {
		action = a;
		return "detailsAction.xhtml";
	}

	public static byte[] readImage(InputStream input) {		
		ByteArrayOutputStream bos = null;		
		try {
			byte[] buffer = new byte[1024];
			bos = new ByteArrayOutputStream();
			for(int i; (i = input.read(buffer)) != -1;)
				bos.write(buffer, 0, i);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bos != null ? bos.toByteArray() : null;		
	}

	public boolean isAdded() {
		return isAdded;
	}
	public void setIsAdded(boolean isAdded) {
		this.isAdded = isAdded;
	}
	public boolean getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		ActionControl.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		ActionControl.description = description;
	}
	public Date getDate1() {
		return date1;
	}

	public void setDate1(Date date1) {
		ActionControl.date1 = date1;
	}

	public Date getDate2() {
		return date2;
	}

	public void setDate2(Date date2) {
		ActionControl.date2 = date2;
	}

	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		ActionControl.amount = amount;
	}
	public boolean getCollectif() {
		return collectif;
	}
	public void setCollectif(boolean collectif) {
		ActionControl.collectif = collectif;
	}
	public boolean getConfirmed() {
		return confirmed;
	}
	public void setConfirmed(boolean confirmed) {
		ActionControl.confirmed = confirmed;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		ActionControl.action = action;
	}

	public Action getActionToUpdate() {
		return actionToUpdate;
	}

	public void setActionToUpdate(Action actionToUpdate) {
		ActionControl.actionToUpdate = actionToUpdate;
	}

	public static boolean getCroissant() {
		return croissant;
	}

	public static void setCroissant(boolean croissant) {
		ActionControl.croissant = croissant;
	}
	public static boolean getDecroissant() {
		return decroissant;
	}
	public static void setDecroissant(boolean decroissant) {
		ActionControl.decroissant = decroissant;
	}
	public String getUrlImage() {
		return urlImage;
	}

	public void setUrlImage(String urlImage) {
		ActionControl.urlImage = urlImage;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		ActionControl.location = location;
	}

	public String getURLMap() {
		return "https://www.google.com/maps/embed/v1/place?key=AIzaSyByoVSQY7PBplx90LH10r3T10kCa00ASdE &q="+this.action.getLocation()+" &maptype=satellite";
	}

	public Part getPartImg() {
		return partImg;
	}

	public void setPartImg(Part partImg) {
		ActionControl.partImg = partImg;
	}


	public Action getActionCarousel1() {
		return gestionActionEJB.getActionsCarrousel().get(0)!=null?gestionActionEJB.getActionsCarrousel().get(0):null;
	}

	public Action getActionCarousel2() {
		return gestionActionEJB.getActionsCarrousel().get(1)!=null?gestionActionEJB.getActionsCarrousel().get(1):getActionCarousel1();
	}

	public Action getActionCarousel3() {
		return gestionActionEJB.getActionsCarrousel().get(2)!=null?gestionActionEJB.getActionsCarrousel().get(2):getActionCarousel2();
	}
	
	public String doDetailsAction1() {
		return this.doDetailsAction(getActionCarousel1());
	}
	
	public String doDetailsAction2() {
		return this.doDetailsAction(getActionCarousel2());
	}
	
	public String doDetailsAction3() {
		return this.doDetailsAction(getActionCarousel3());
	}

	
}