package controller;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import entities.Action;

@Named
@RequestScoped
public class NavigationControl {

	public NavigationControl(){
	}
	
	public String goToAbout() {
		return "about.xhtml";
	}
	
	public String goToIndex() {
		return "index.xhtml";
	}
	
	public String goToCreateEvent() {
		return "createEvent.xhtml";
	}
	
	public String goToListEvent() {
		return "listEvent.xhtml";
	}
	
	public String goToCreateAction() {
		return "createAction.xhtml";
	}
	
	public String goToCreateActionUser() {
		return "createActionUser.xhtml";
	}
	
	public String goToListAction() {
		return "listAction.xhtml";
	}
	public String goToListActionUser() {
		return "listActionUser.xhtml";
	}
	
	public String goToLogin() {
		return "login.xhtml";
	}
	
	public String goToAdmin() {
		return "admin.xhtml";
	}
	
	public String deconnexion() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
	    session.invalidate();
		return "index.xhtml?faces-redirect=true";
	}
	
	public boolean isConnected() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
	    return session !=null;
	}

	public String styleConnected() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		return session!=null?"display:block":"display:none";
	}
	
	public String styleDisconnected() {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		return session!=null?"display:none":"display:block";
	}
	
	public String getUserName() {
		return FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal().getName();
	}
	
}
