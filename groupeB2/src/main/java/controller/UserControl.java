package controller;

import java.util.List;

import javax.ejb.EJB;

import entities.User;
import sessionejb.GestionUserEJB;


public class UserControl {
	
	@EJB
	public GestionUserEJB gestionUserEJB;
	//controleur
	private boolean isAdded;
	private boolean isDelete;
	private String login,psw;
	
	public UserControl() {}

	public List<User> selectAll() {
		return gestionUserEJB.selectAll();
	}
	
	public boolean doAddEvent() {
		isAdded = gestionUserEJB.addUser(new User(login,psw));
		return isAdded;
	}
	
	public boolean doDeleteEvent(Integer id) {
		isDelete = gestionUserEJB.deleteUser(id);
		return isDelete;
	}
	
	public boolean isAdded() {
		return isAdded;
	}
	
	public void setAdded(boolean isAdded) {
		this.isAdded = isAdded;
	}
	
	public boolean isDelete() {
		return isDelete;
	}
	
	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login2) {
		this.login = login2;
	}
	public String getPsw() {
		return psw;
	}
	public void setPsw(String psw2) {
		this.psw = psw2;
	}

}