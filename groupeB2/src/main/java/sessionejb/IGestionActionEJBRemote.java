package sessionejb;

import java.util.List;

import javax.ejb.Remote;

import entities.Action;

@Remote
public interface IGestionActionEJBRemote {
	
	List<Action> selectAll();
	Action get(Integer id);
	boolean addAction(Action a);
	boolean deleteAction(Integer id);
	boolean updateAction(Action action);
}
