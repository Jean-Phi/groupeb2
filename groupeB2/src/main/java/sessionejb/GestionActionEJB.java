package sessionejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import dao.ActionDAO;
import entities.Action;

@Stateless
@LocalBean
public class GestionActionEJB implements IGestionActionEJBRemote {

	@EJB
	private  ActionDAO dao;
	
	@Override
	public List<Action> selectAll() {
		return dao.selectAllActions();
	}

	@Override
	public boolean addAction(Action a) {
		return dao.addAction(a);
	}

	@Override
	public Action get(Integer id) {
		return dao.getActionById(id);
	}

	@Override
	public boolean deleteAction(Integer id) {
		return dao.deleteActionId(id);
	}

	@Override
	public boolean updateAction(Action action) {
		return dao.updateAction(action);
	}

	public List<Action> getActionsCarrousel() {
		return dao.getActionsCarrousel();
	}

}
