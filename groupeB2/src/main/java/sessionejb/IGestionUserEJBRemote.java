package sessionejb;

import java.util.List;

import javax.ejb.Remote;

import entities.User;
@Remote
public interface IGestionUserEJBRemote {
	
	List<User> selectAll();
	User get(Integer id);
	User get(String login, String password);
	boolean addUser(User user);
	boolean deleteUser(Integer id);
	boolean updateUser(User user);
}
