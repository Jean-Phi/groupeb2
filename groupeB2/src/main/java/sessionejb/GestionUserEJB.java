package sessionejb;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import dao.UserDAO;
import entities.User;

@Stateless
@LocalBean
public class GestionUserEJB implements IGestionUserEJBRemote{
	
	@EJB
	private UserDAO dao;

	@Override
	public List<User> selectAll() {
		return dao.getUsers();
	}

	@Override
	public User get(Integer id) {
		return dao.getUserById(id);
	}

	@Override
	public boolean addUser(User user) {
		return dao.addUser(user);
	}

	@Override
	public boolean deleteUser(Integer id) {
		return dao.deleteUserId(id);
	}

	@Override
	public boolean updateUser(User user) {
		return dao.updateUser(user);
	}

	@Override
	public User get(String login, String password) {
		return dao.getUserByLogin(login,password);
	}

}
